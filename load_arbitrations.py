#!/usr/bin/python
# -*- coding: utf-8 -*-
import json, os
import textract
import requests
import config

params = (
    ('arbitration_awards_only', 'true'),
    ('dir', 'desc'),
    ('for_user', 'true'),
    ('page', '0'),
    ('records', '1000'),
    ('sorting', 'borrower_name'),
)

# the script
arbitrationsPath = os.path.join('.','arbitrations')
arbitrationsJsonPath = os.path.join(arbitrationsPath,'arbitrations.json')
try:
    os.mkdir(arbitrationsPath)
except OSError:
    pass
try:
    jsonFile = open(arbitrationsJsonPath).read()
except IOError:
    resp = requests.get('https://btcjam.com/listing_investments.json', headers=config.headers, params=params, cookies=config.cookies, stream=True)
    with open(arbitrationsJsonPath, 'wb') as fd:
        for chunk in resp.iter_content():
            fd.write(chunk)
    jsonFile = open(arbitrationsJsonPath).read()
jsonData = json.loads(jsonFile)
# print json.dumps(jsonData, indent=4)

amountLeftTotal = 0.0
amountOfDefaulted = 0
amountOfRepaid = 0
amountLeftTotalByArbitrationDocuments = 0.0
for obj in jsonData:
    amountLeftTotal = amountLeftTotal + float(obj['amount_left'])
    if obj['payment_state'] == 'defaulted': amountOfDefaulted = amountOfDefaulted + 1
    if obj['payment_state'] == 'Repaid': amountOfRepaid = amountOfRepaid + 1
    pdfPath = os.path.join(arbitrationsPath, str(obj['id'])+'.pdf')
    if not os.path.isfile(pdfPath):
        print 'Downloading arbitration id = %d' % (obj['id'])
        r = requests.get('https://btcjam.com/' + obj['arbitration_award_url'],
                         headers=config.headers, cookies=config.cookies, stream=True)
        with open(pdfPath, 'wb') as fd:
            for chunk in r.iter_content():
                fd.write(chunk)
    if obj['payment_state'] == 'defaulted':
        text = textract.process(pdfPath)
        text = text.split('\n')
        for i,line in enumerate(text):
            if '=>' in line:
                creditor = text[i+1].replace('\n','').strip()
                amountLineInPdf = line.replace('\n','').split('=>')[1].strip()
                amountInPdf = float(amountLineInPdf.split(' ')[0])
                print pdfPath
                print '%s <= %s' % (creditor, amountLineInPdf)
                amountLeftTotalByArbitrationDocuments = amountLeftTotalByArbitrationDocuments + amountInPdf
print ('-'*70)
print 'From JSON data: Total amount left (to be paid) => %f bitcoins' % (amountLeftTotal)
print ('-'*70)
print 'From PDF Arbitration data (still defaulted): Total amount due => %f bitcoins' % (amountLeftTotalByArbitrationDocuments)
print ('-'*70)
print 'Total amount of defaulted investements => %d / %d' % (amountOfDefaulted, len(jsonData))
print ('-'*70)
print 'Total amount of Repaid investements => %d / %d' % (amountOfRepaid, len(jsonData))
print ('-'*70)
