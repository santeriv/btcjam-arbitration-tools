# README #

- login to btcjam.com with Chrome https://btcjam.com
- Open Chrome DevTools
- Open Investments page https://btcjam.com/listing_investments
- Click the checkbox 'ARBITRATION AWARDS ONLY'
- From Network panel
  - right-click on JSON request 'https://btcjam.com/listing_investments.json?arbitration_award...'
  - Choose 'Copy' -> 'Copy as a cURL'
- When copied the command (clipboard) go to https://curl.trillworks.com/ and paste it there and copy pythonized 'cookies' and 'headers' to config.py
  - In case you want to see source code (or run it by yourself): https://github.com/NickCarneiro/curlconverter
- To get started run:
```
# tested with linux ubuntu 16.10 python 2.7
sudo apt install libpulse-dev
sudo pip install -r requirements.txt
# modify config.py (see above)
git update-index --assume-unchanged config.py
./load_arbitrations.py > load_arbitrations.log
```